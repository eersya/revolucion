new Vue({
  el: '#EmpresaList',
  delimiters: ['${','}'],
  data: {
    empresaList: [],
    currentEmpresa: {},
    search_term: '',
    loading: true,
  },
  methods: {
    getEmpresaList: function() {
      let api_url = '/gestor/api/empresa/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.empresaList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getEmpresaUrl: function(empresa) {
      return "/gestor/empresa/empresa_rfc/".replace('empresa_rfc', empresa.rfc)
    }
  },
  mounted: function() {
    this.getEmpresaList();
  },
});
