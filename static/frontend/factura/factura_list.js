new Vue({
    el: '#CotizacionFacturaList',
    delimiters: ['${','}'],
    data: {
      cotizacionFacturaList: [],
      currentFactura: {},
      search_term: '',
      estado: {number: 2},
      suma_subtotal: '0.00',
      suma_total: '0.00',
      suma_ganancia: '0.00',
      loading: true,
    },
    methods: {
      getCotizacionFacturaList: function() {
        let api_url = '/gestor/api/cotizacion/?is_factura=true';
        if(this.estado.number !== 2) {
          api_url = api_url.concat('&factura__estado='.concat(String(this.estado.number)));
        }
        if(this.search_term !== '' || this.search_term !== null) {
          api_url = api_url.concat('&search='.concat(this.search_term));
        }
        console.log(api_url);
        
        this.$http.get(api_url)
          .then((response) => {
            this.cotizacionFacturaList = response.data;
            this.getSumas();
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
      },
      getSumas: function() {
        var suma_ganancia = 0.00;
        var suma_total = 0.00;
        var suma_subtotal = 0.00;
        for (i = 0; i < this.cotizacionFacturaList.length; ++i) {
          suma_subtotal += Number(this.cotizacionFacturaList[i].subtotal);
          suma_total += Number(this.cotizacionFacturaList[i].total);
          suma_ganancia += Number(this.cotizacionFacturaList[i].ganancia);
        };
        
        this.suma_ganancia = suma_ganancia.toFixed(2);
        this.suma_subtotal = suma_subtotal.toFixed(2);
        this.suma_total = suma_total.toFixed(2);
      },
      getContactoNombre: function(cotizacion) {
        if(cotizacion.contacto === null) {
          return '';
        }
        return cotizacion.contacto.nombre;
      },
      getUsuarioUsername: function(cotizacion) {
        if(cotizacion.usuario === null) {
          return '';
        }
        return cotizacion.usuario.username;
      },
      getEmpresaRazonSocial: function(cotizacion) {
        this.$http.get('/gestor/api/empresa/'.concat(cotizacion.empresa))
          .then((response) => {
            this.loading = false;
            return response.data.razon_social;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
          return '';
      },
      getCotizacionFacturaURL: function(cotizacion) {
        return "/gestor/empresa/empresa_rfc/factura/factura_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('factura_id', cotizacion.factura.id);
      },
      getCotizacionUrl: function(cotizacion) {
        return "/gestor/empresa/empresa_rfc/cotizacion/cotizacion_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('cotizacion_id', cotizacion.id);
      },
      getCotizacionEmpresaUrl: function(cotizacion) {
        return "/gestor/empresa/empresa_rfc/".replace('empresa_rfc', cotizacion.empresa.rfc);
      },
      getCotizacionContactoUrl: function(cotizacion) {
        if(cotizacion.contacto === null) {
          return '';
        }
        return "/gestor/empresa/empresa_rfc/contacto/contacto_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('contacto_id', cotizacion.contacto.id);
      },
    },
    mounted: function() {
      this.getCotizacionFacturaList();
    },
  });