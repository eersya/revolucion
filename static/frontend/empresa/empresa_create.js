new Vue({
    el: '#EmpresasAsociadas',
    delimiters:['${', '}'],
    data: {
      empresaList: [],
      currentEmpresa: {},
      currentEmpresaRFC: '',
      selectedEmpresas: [],
      search_term: '',
      loading: false,
    },
    methods: {
      getEmpresaList: function(){
        this.loading = true;
        let api_url = '/gestor/api/empresa/';
        if(this.search_term !== '' || this.search_term !== null) {
          api_url = api_url.concat('?search='.concat(this.search_term));
        }
        this.$http.get(api_url)
          .then((response) => {
            this.empresaList = response.data;
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
      },
      getEmpresa: function(rfc){
        this.loading = true;
        console.log('/gestor/api/empresa/empresa_rfc/'.replace('empresa_rfc', rfc));
        this.$http.get('/gestor/api/empresa/empresa_rfc/'.replace('empresa_rfc', rfc))
          .then((response) => {
            this.currentEmpresa = response.data;
            this.currentEmpresaRFC = this.currentEmpresa.rfc;
            this.selectedEmpresas.push(this.currentEmpresaRFC);
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
      },
    }
  });
  