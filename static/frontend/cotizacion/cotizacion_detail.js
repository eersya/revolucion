new Vue({
  el: '#BuscarProducto',
  delimiters: ['${','}'],
  data: {
    productoSearchList: [],
    search_term: '',
    loading: true,
  },
  methods: {
    searchProducto: function() {
      let api_url = '/gestor/api/producto/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.productoSearchList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
        })
    },
    getProductoAddUrl: function(empresa_rfc, cotizacion_id, producto) {
      return "/gestor/empresa/empresa_rfc/cotizacion/cotizacion_id/producto/producto_id/agregar/".replace('empresa_rfc', empresa_rfc).replace('cotizacion_id', cotizacion_id).replace('producto_id', producto.id)
    },
  },
  mounted: function() {
    this.searchProducto();
  },
});
