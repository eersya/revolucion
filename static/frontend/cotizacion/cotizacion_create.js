new Vue({
    el: '#BuscarContacto',
    delimiters:['${', '}'],
    data: {
      contactoList: [],
      currentContacto: {},
      currentContactoStr: '',
      search_term: '',
      loading: false,
    },
    methods: {
      getContactoList: function(empresa_rfc){
        this.loading = true;
        let api_url = '/gestor/api/contacto/?search=empresa_rfc'.replace('empresa_rfc', empresa_rfc);
        if(this.search_term !== '' || this.search_term !== null) {
          api_url = api_url.concat('+'.concat(this.search_term));
        }
        this.$http.get(api_url)
          .then((response) => {
            this.contactoList = response.data;
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
      },
      getContacto: function(id){
        this.loading = true;
        this.$http.get('/gestor/api/contacto/contacto_id/'.replace('contacto_id', id))
          .then((response) => {
            this.currentContacto = response.data;
            this.currentContactoStr = 'id'.replace('id',this.currentContacto.id);
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          });
      },
    }
  });
  