new Vue({
  el: '#CotizacionList',
  delimiters: ['${','}'],
  data: {
    cotizacionList: [],
    currentCotizacion: {},
    search_term: '',
    suma_subtotal: '',
    suma_total: '',
    suma_ganancia: '',
    loading: true,
  },
  methods: {
    getCotizacionList: function() {
      let api_url = '/gestor/api/cotizacion/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.cotizacionList = response.data;
          this.getSumas();
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getSumas: function() {
      var suma_ganancia = 0.00;
      var suma_total = 0.00;
      var suma_subtotal = 0.00;
      for (i = 0; i < this.cotizacionList.length; ++i) {
        suma_subtotal += Number(this.cotizacionList[i].subtotal);
        suma_total += Number(this.cotizacionList[i].total);
        suma_ganancia += Number(this.cotizacionList[i].ganancia);
      };
      
      this.suma_ganancia = suma_ganancia.toFixed(2);
      this.suma_subtotal = suma_subtotal.toFixed(2);
      this.suma_total = suma_total.toFixed(2);
    },
    getContactoNombre: function(cotizacion) {
      if(cotizacion.contacto === null) {
        return '';
      }
      return cotizacion.contacto.nombre;
    },
    getUsuarioUsername: function(cotizacion) {
      if(cotizacion.usuario === null) {
        return '';
      }
      return cotizacion.usuario.username;
    },
    getEmpresaRazonSocial: function(cotizacion) {
      this.$http.get('/gestor/api/empresa/'.concat(cotizacion.empresa))
        .then((response) => {
          this.loading = false;
          return response.data.razon_social;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
        return '';
    },
    getCotizacionUrl: function(cotizacion) {
      return "/gestor/empresa/empresa_rfc/cotizacion/cotizacion_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('cotizacion_id', cotizacion.id);
    },
    getCotizacionEmpresaUrl: function(cotizacion) {
      return "/gestor/empresa/empresa_rfc/".replace('empresa_rfc', cotizacion.empresa.rfc);
    },
    getCotizacionContactoUrl: function(cotizacion) {
      if(cotizacion.contacto === null) {
        return '';
      }
      return "/gestor/empresa/empresa_rfc/contacto/contacto_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('contacto_id', cotizacion.contacto.id);
    },
  },
  mounted: function() {
    this.getCotizacionList();
  },
});
