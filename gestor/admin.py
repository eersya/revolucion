from django.contrib import admin

from .models import Empresa, EmpresaArchivo, Contacto
from .models import Producto, CatalogoClaveProducto
from .models import Cotizacion, Factura, ProductoCotizado, FacturaArchivos

admin.site.register(Empresa)
admin.site.register(EmpresaArchivo)
admin.site.register(Contacto)

admin.site.register(Producto)
admin.site.register(CatalogoClaveProducto)

admin.site.register(Cotizacion)
admin.site.register(ProductoCotizado)
admin.site.register(Factura)
admin.site.register(FacturaArchivos)
