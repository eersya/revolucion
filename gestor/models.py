from django.db import models

from django.contrib.auth.models import User

from .utils import empresa_directory_path
from .utils import update_producto_cotizado, update_cotizacion
from .utils import get_descripcion

# Dictionaries
class CatalogoClaveProducto(models.Model):
    clave = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=20)

    def __str__(self):
        return '%d' % (self.clave)

#Datos Escenciales para el funcionamiento

class Empresa(models.Model):
    rfc = models.CharField(
        max_length=13,
        primary_key=True,
    )
    razon_social = models.CharField(max_length=40)
    nombre_comercial = models.CharField(
        max_length=40,
        blank=True, 
        null=True,
    )
    correo = models.EmailField(max_length=40)
    telefono = models.CharField(max_length=22)
    BANCOS = (
        (1, 'Bancomer'),
        (2, 'Banamex'),
        (3, 'Banorte'),
        (3, 'IXE'),
        (0, 'N/A'),
    )
    banco = models.IntegerField(
        choices=BANCOS,
        default=0,
    )
    referencia_bancaria = models.IntegerField(default = -1)
    CUIDAD_ESTADO = (
        (1, 'CDMX/CDMX'),
        (2, 'Estado de México/Toluca'),
    )
    USOS_CDFI = (
        ('P01', 'Por definir'),
        ('D10', 'Pagos por servicios educativos (colegiaturas)'),
        ('D09', 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones'),
        ('D08', 'Gastos de transportación escolar obligatoria'),
        ('D07', 'Primas por seguros de gastos médicos'),
        ('D06', 'Aportaciones voluntarias al SAR'),
        ('D05', 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)'),
        ('D04', 'Donativos'),
        ('D03', 'Gastos funerales'),
        ('D02', 'Gastos médicos por incapacidad o discapacidad'),
        ('D01', 'Honorarios médicos, dentales y gastos hospitalarios'),
        ('I08', 'Otra maquinaria y equipo'),
        ('I07', 'Comunicaciones satelitales'),
        ('I06', 'Comunicaciones telefónicas'),
        ('I05', 'Dados, troqueles, moldes, matrices y herramental'),
        ('I04', 'Equipo de cómputo y accesorios'),
        ('I03', 'Equipo de transporte'),
        ('I02', 'Mobiliario y equipo de oficina por inversiones'),
        ('I01', 'Construcciones'),
        ('G03', 'Gastos en general'),
        ('G02', 'Devoluciones, descuentos o bonificaciones'),
        ('G01', 'Adquisición de mercancías'),
    )
    uso_cdfi = models.CharField(
        max_length=3,
        choices=USOS_CDFI,
        default='P01',
    )
    FORMAS_PAGO = (
        (0, 'Efectivo'),
        (1, 'Cheque nominativo'),
        (2, 'Transferencia electrónicade fondos'),
        (3, 'Por definir'),
    )
    forma_pago = models.IntegerField(
        choices=FORMAS_PAGO,
        default=0,
    )
    METODOS_PAGO = (
        (0, 'PUE'),
        (1, 'PPD'),
    )
    metodo_pago = models.IntegerField(
        choices=METODOS_PAGO,
        default=0,
    )
    TIPOS_COMPROBANTE = (
        (0, 'I'),
        (1, 'E'),
        (2, 'T'),
        (3, 'P'),
        (4, 'N'),
    )
    tipo_comprobante = models.IntegerField(
        choices=TIPOS_COMPROBANTE,
        default=0,
    )
    cuidad_estado = models.IntegerField(
        choices=CUIDAD_ESTADO,
        default=1,
    )
    cp = models.IntegerField(default=-1)
    direccion = models.CharField(
        max_length=100,
        blank=True, 
        null=True,
    )
    direccion_entrega = models.CharField(
        max_length=100,
        blank=True, 
        null=True,
    )
    empresas_asociadas = models.ManyToManyField(
        "Empresa",
        related_name='emprsas_asociadas',
        blank=True, 
    )
    extra_1 = models.TextField(
        max_length=150,
        blank=True, null=True,
    )
    extra_2 = models.TextField(
        max_length=150,
        blank=True, null=True,
    )
    extra_3 = models.TextField(
        max_length=150,
        blank=True, null=True,
    )

    def __str__(self):
        return '%s' % self.rfc

class EmpresaArchivo(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        on_delete=models.CASCADE,
        related_name='archivos',
    )
    archivo = models.FileField(upload_to=empresa_directory_path)
    fecha = models.DateField(auto_now_add=True)


class Contacto(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        on_delete=models.CASCADE,
    )
    nombre = models.CharField(max_length=23)
    correo = models.EmailField(max_length=40)
    extension = models.CharField(
        max_length=5,
        blank=True, 
        null=True,
    )
    telefono = models.CharField(max_length=20)
    celular = models.CharField(
        max_length=20,
        blank=True, 
        null=True,
    )
    PREFIJO_CHOICES = (
        (0, ''),
        (1, 'Sr.'),
        (2, 'Sra.'),
        (3, 'Lic.'),
        (4, 'Ing.'),
    )
    prefijo = models.IntegerField(
        choices=PREFIJO_CHOICES,
        default=0,
    )
    puesto = models.CharField(
        max_length=20,
    )
    extra = models.TextField(
        max_length=150,
        blank=True, 
        null=True,
    )

    def __str__(self):
        return '%d[Nombre:%s]' % (self.id, self.nombre)

# Producto

class Producto(models.Model):
    numero_parte = models.CharField(max_length=40)
    TIPOS_PRODUCTO = (
        ('Prd', 'Producto'),
        ('Srv', 'Servicio'),
    )
    tipo_producto = models.CharField(
        max_length=3,
        choices=TIPOS_PRODUCTO,
        default='Prd',
    )
    UNIDADES_MEDIDA = (
        ('H87', 'Pieza'),
        ('XBX', 'Caja'),
        ('O', 'Otro'),
        ('0', 'No Aplica'),
    )
    unidad_medida = models.CharField(
        max_length=3,
        choices=UNIDADES_MEDIDA,
        default='H87',
    )
    clave = models.ForeignKey(
        CatalogoClaveProducto,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    ALIMENTO_IVA = 0
    SIN_ESPECIFICAR_IVA = 16
    TIPOS_IVA = (
        (ALIMENTO_IVA, 'ALIMENTO(0%)'),
        (SIN_ESPECIFICAR_IVA, 'S/A(16%)'),
    )
    tipo_iva = models.IntegerField(
        choices=TIPOS_IVA,
        default=SIN_ESPECIFICAR_IVA,
    )
    descripcion = models.TextField(
        max_length=100,
        blank=True,
    )


# Cotización

class Cotizacion(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        on_delete=models.SET_NULL,
        blank=True, 
        null=True,
        related_name='cotizaciones',
    )
    usuario = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    referencia = models.CharField(
        max_length=20,
        blank=True, 
        null=True,
    )
    fecha = models.DateField(auto_now_add=True)
    contacto = models.ForeignKey(
        Contacto,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    costo_operacion = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    monto_iva = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    subtotal = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    ganancia = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    margen_ganancia = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    inversion = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    total = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    is_factura = models.BooleanField(default=False)
    extras = models.TextField(
        max_length=300,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%d[Nombre:%s]' % (self.id,self.referencia)

    def save(self, *args, **kwargs):
        update_cotizacion(self)
        super(Cotizacion, self).save(*args, **kwargs)

# Cuando se cotize un producto, en costo debera de aparecer el costo del producto
# junto con el respectivo margen de ganancia
class ProductoCotizado(models.Model):
    cotizacion = models.ForeignKey(
        Cotizacion,
        on_delete=models.CASCADE,
        related_name='productos_cotizados',
    )
    producto = models.ForeignKey(
        Producto,
        on_delete = models.CASCADE,
    )
    provedor = models.ForeignKey(
        Empresa,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    disponibilidad = models.CharField(
        max_length=10,
    )
    precio = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    costo = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    margen_ganancia = models.PositiveIntegerField(default=0)
    cantidad = models.PositiveIntegerField(default=1)
    monto_iva = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=-1.00,
    )
    importe = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    descripcion = models.TextField(
        max_length=150,
        blank=True,
    )

    def get_descripcion(instance):
        return instance.producto.descripcion

    def __str__(self):
        return '%d[Cotizacion:%d]' % (self.id, self.cotizacion.id)

    def save(self, *args, **kwargs):
        update_producto_cotizado(self)
        super(ProductoCotizado, self).save(*args, **kwargs)
        cotizacion = self.cotizacion
        update_cotizacion(cotizacion)
        cotizacion.save()

    def delete(self, *args, **kwargs):
        cotizacion = self.cotizacion
        super(ProductoCotizado, self).delete(*args, **kwargs)
        update_cotizacion(cotizacion)
        cotizacion.save()


class Factura(models.Model):
    cotizacion = models.OneToOneField(
        Cotizacion,
        on_delete=models.CASCADE,
        related_name='factura',
    )
    firma = models.CharField(max_length=30)
    monto_pagado = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    ESTADOS = (
        (-1, 'Cancelada'),
        (0, 'En curso'),
        (1, 'Pagada'),
    )
    estado = models.IntegerField(
        choices=ESTADOS,
        default=0,
    )
    fecha_pago = models.DateField(
        blank=True, 
        null=True,
    )

    def __str__(self):
        return '%d[Cotizacion:%d][Empresa:%s]' % (self.id, self.cotizacion.id, self.cotizacion.empresa.rfc)

class FacturaArchivos(models.Model):
    factura = models.ForeignKey(
        Factura,
        on_delete=models.CASCADE,
        related_name='factura_archivos',
    )
    archivo = models.FileField(upload_to='factura/%d/')
