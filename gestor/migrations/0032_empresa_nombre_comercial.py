# Generated by Django 2.0.8 on 2018-08-03 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestor', '0031_productocotizado_disponibilidad'),
    ]

    operations = [
        migrations.AddField(
            model_name='empresa',
            name='nombre_comercial',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
    ]
