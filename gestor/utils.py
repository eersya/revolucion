from io import BytesIO
from decimal import Decimal

from django.http import HttpResponse
from django.template.loader import get_template

from django.utils import timezone

def update_producto_cotizado(producto_cotizado):
    if not producto_cotizado.producto is None:
        iva = producto_cotizado.producto.tipo_iva
        producto_cotizado.precio = producto_cotizado.costo + Decimal(producto_cotizado.costo) * Decimal(producto_cotizado.margen_ganancia/100)
        producto_cotizado.monto_iva = producto_cotizado.precio * Decimal(iva / 100) * Decimal(producto_cotizado.cantidad)
    else:
        producto_cotizado.precio = producto_cotizado.costo
    producto_cotizado.importe = producto_cotizado.precio * Decimal(producto_cotizado.cantidad)

def update_cotizacion(cotizacion):
    cotizacion.monto_iva = Decimal(0)
    cotizacion.subtotal = Decimal(0)
    cotizacion.total = Decimal(0)
    cotizacion.subtotal = Decimal(0)
    cotizacion.inversion = Decimal(0)
    cotizacion.margen_ganancia = Decimal(0)
    for producto_cotizado in cotizacion.productos_cotizados.all():
        cotizacion.monto_iva += producto_cotizado.monto_iva
        cotizacion.subtotal += producto_cotizado.importe
        cotizacion.inversion += Decimal(producto_cotizado.costo * producto_cotizado.cantidad)
    cotizacion.inversion += Decimal(cotizacion.costo_operacion)
    if not cotizacion.inversion == 0:
        cotizacion.margen_ganancia = (Decimal(cotizacion.ganancia) / cotizacion.inversion) * 100
    cotizacion.total = cotizacion.monto_iva + cotizacion.subtotal

def empresa_directory_path(instance, filename):
    return 'empresa_{0}/{1}_{2}'.format(instance.empresa.rfc, timezone.now() ,filename)

def get_descripcion(instance):
    return instance.producto.descripcion
